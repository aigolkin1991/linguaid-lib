package interfacelib

import "time"

type IHeartbeat interface {
	ReadinessProbe(timeout time.Duration) error
	LivenessProbe(interval time.Duration) error
}
