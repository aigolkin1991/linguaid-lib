package interfacelib

type ChanelName = string

type IPubSub interface {
	Publish(ch ChanelName, msg []byte) error
	Subscribe(ch ChanelName) *chan []byte
}
