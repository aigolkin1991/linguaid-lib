package interfacelib

import "time"

type KeyValueStoreKey = string

type IKeyValueStore interface {
	SetExKey(key KeyValueStoreKey, value []byte, expire time.Duration) error
	GetKey(key KeyValueStoreKey) (interface{}, error)
}
