package interfacelib

type IPersistanStorage[T any] interface {
	Create(T) error
	Get(id []byte) T
	Delete(id []byte) bool
}
